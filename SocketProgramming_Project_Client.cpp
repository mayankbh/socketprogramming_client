//============================================================================
// Name        : SocketProgramming_Project_Client.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================
#include <arpa/inet.h>
#include <curses.h>
#include <netinet/in.h>
#include <strings.h>
#include <string>
#include <vector>
#include <json/json.h>
#include <json/writer.h>
#include <json/reader.h>
#include <iostream>
#include <cstring>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/fcntl.h>
#include <cstdio>
#include <signal.h>
#include <boost/algorithm/string.hpp>
#include <errno.h>
using namespace std;

//TODO fix the page count bug

#define PRINT_DEBUG(fmt, ...) \
		wprintw(debug_window, fmt, ##__VA_ARGS__); wrefresh(debug_window);

#define CONNECT 'c'
#define DISCONNECT 'd'
#define REFRESH 'f'
#define NEXT_PAGE 'n'
#define PREV_PAGE 'p'
#define REGISTER_SONG 'r'
#define UNREGISTER_SONG 'u'
#define SEARCH_SONG 's'
#define QUIT 'q'

#define REGISTER_PORT 9090
#define SEARCH_PORT 9091
#define GET_PEER_PORT 9092

#define SONGS_PER_PAGE 10

vector<string> song_list;
vector<string> key_list;
struct sockaddr_in current_server_registration_address;
struct sockaddr_in current_server_lookup_address;
struct sockaddr_in current_server_peer_address;
WINDOW *debug_window;

int current_song_list_page = 0;
int max_page_index;
map<string, string> song_map;
vector<Json::Value *> element_list;
Json::Value initial_list_root(Json::arrayValue);

int socket_manager_request_pipefd;
int socket_manager_result_pipefd;
int player_command_pipefd;

pid_t socket_manager_pid;
pid_t player_pid;

void send_play_request(Json::Value value)
{
	PRINT_DEBUG("Sending play request\n");
	value["Command"] = "Play";

	Json::FastWriter writer;
	string str = writer.write(value);
	write(player_command_pipefd, str.c_str(), str.length());
}

void add_song_to_list(Json::Value value)
{
	//TODO Need to validate JSON somewhere
	//Otherwise exception will be thrown

	//TODO Also fix this drawing, add more metadata to the string
	song_list.push_back(value["Artist"].asString() + " - " + value["Title"].asString());
	string key = value["Key"].asString();
	key_list.push_back(key);
}

void update_song_menu(WINDOW *song_window)
{
	//Redraw based on current page
	wclear(song_window);
	wprintw(song_window, "Song list\n");
	int start_index = current_song_list_page * SONGS_PER_PAGE;

	vector<string>::iterator songIterator = song_list.begin() + start_index;
	int counter = 0;
	while(counter < SONGS_PER_PAGE && songIterator != song_list.end())
	{
		//1, 2, 3 followed by song details
		wprintw(song_window, "%d)", (counter + 1) % SONGS_PER_PAGE);
		wprintw(song_window, songIterator->c_str());
		wprintw(song_window, "\n");
		songIterator++;
		counter++;
	}
	wrefresh(song_window);

}

void handle_next_page(WINDOW *song_window, WINDOW *io_menu)
{
	if(current_song_list_page < max_page_index)
	{
		current_song_list_page++;
		wclear(io_menu);
		wprintw(io_menu, "Page %d/%d", current_song_list_page+1, max_page_index+1);
		wrefresh(io_menu);
	}
	else
	{
		wclear(io_menu);
		wprintw(io_menu, "Already on last page");
		wrefresh(io_menu);
	}
	update_song_menu(song_window);
}

void handle_prev_page(WINDOW *song_window, WINDOW *io_menu)
{
	if(current_song_list_page > 0)
	{
		current_song_list_page--;
		wclear(io_menu);
		wprintw(io_menu, "Page %d/%d", current_song_list_page+1, max_page_index+1);
		wrefresh(io_menu);
	}
	else
	{
		wclear(io_menu);
		wprintw(io_menu, "Already on first page");
		wrefresh(io_menu);
	}
	update_song_menu(song_window);
}

void update_song_list(Json::Value root, WINDOW *song_window)
{
	//Clear the existing song list
	song_list.clear();
	key_list.clear();

	if(root.isArray())
	{
		for(Json::ValueIterator iterator = root.begin(); iterator != root.end(); iterator++)
		{
			//Add to the song list vector
			add_song_to_list(*iterator);
		}

		current_song_list_page = 0;
		max_page_index = song_list.size() / SONGS_PER_PAGE;
		update_song_menu(song_window);
	}
	else
	{
		//Error handling
	}
}

void display_menu(WINDOW *menu_window)
{
	wprintw(menu_window, "%c)Connect\n", CONNECT);
	wprintw(menu_window, "%c)Disconnect\n", DISCONNECT);
	wprintw(menu_window, "%c)Song list - Refresh\n", REFRESH);
	wprintw(menu_window, "%c)Song list - Next page\n", NEXT_PAGE);
	wprintw(menu_window, "%c)Song list - Previous page\n", PREV_PAGE);
	wprintw(menu_window, "%c)Register song\n", REGISTER_SONG);
	wprintw(menu_window, "%c)Unregister song\n", UNREGISTER_SONG);
	wprintw(menu_window, "%c)Search for song\n", SEARCH_SONG);
	wprintw(menu_window, "%c)Quit\n", QUIT);
	wrefresh(menu_window);
}

void do_connect(WINDOW *io_menu)
{
	//TODO Placeholder connect until connection semantics are finalized
	wclear(io_menu);
	wprintw(io_menu, "Successfully connected to server");
	wrefresh(io_menu);
}

void handle_disconnect(WINDOW *io_menu)
{
	//TODO Placeholder disconnect until disconnection semantics are finalized
	wclear(io_menu);
	wprintw(io_menu, "Successfully disconnected from server");
	wrefresh(io_menu);
}

void handle_connect(WINDOW *io_menu)
{
	char buf[20];

	bzero(buf, 20);
	wclear(io_menu);
	wprintw(io_menu, "IP Address : ");
	wrefresh(io_menu);
	curs_set(1);
	echo();

	//Try to get input
	wgetstr(io_menu, buf);

	//Attempt to parse it
	if(inet_aton(buf, &current_server_registration_address.sin_addr) == 0)
	{
		//Display an error
		wclear(io_menu);
		wprintw(io_menu, "Unable to parse IP : %s" , buf);
		wrefresh(io_menu);
	}
	else
	{
		do_connect(io_menu);
	}
	curs_set(0);
	noecho();
}

Json::Value generate_search_json(char *buf)
{
	//Create a new empty object
	Json::Value root(Json::objectValue);
	string str_val(buf);
	//TODO Currently only processes title
	root["Title"] = str_val;

	return root;
}

void do_search(WINDOW *io_menu,
				WINDOW *song_window,
				Json::Value root
				)
{
	//TODO Buffer size might need to be changed
	//or even dynamic
	char recvbuf[10000];

	int bytes_to_send;
	Json::FastWriter writer;
	string json_object_as_string = writer.write(root);
	const char *buf = json_object_as_string.c_str();
	bytes_to_send = strlen(buf);

	//TODO wrap all socket send/receive calls in a decent wrapper
	//Since we know all payload data is JSON
	//Create a socket, transmit JSON object over it
	int sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if(sockfd < 0)
	{
		wclear(io_menu);
		wprintw(io_menu, "Failed to create socket");
		wrefresh(io_menu);
	}

	if(connect(sockfd, (struct sockaddr *)&current_server_lookup_address, sizeof(struct sockaddr)) < 0)
	{
		wclear(io_menu);
		wprintw(io_menu, "Failed to connect to server");
		wrefresh(io_menu);
		return;
	}
	int bytes_sent = 0;
	//Keep sending until all bytes sent
	//TODO Error handling needed here
	while(bytes_to_send > 0)
	{
		bytes_sent = send(sockfd, buf + bytes_sent, bytes_to_send, 0);
		bytes_to_send -= bytes_sent;
	}
	bzero(recvbuf, 1024);
	//Close the write end of the socket so that the server receives an
	//end of stream
	shutdown(sockfd, SHUT_WR);
	int bytes_received = 0;
	while(true)
	{
		bytes_received = recv(sockfd, recvbuf + bytes_received, 10000 - bytes_received, 0);
		if(bytes_received == 0)	//Server closed connection
		{
			break;
		}
	}
	close(sockfd);

	//Now parse the payload
	Json::Reader reader;
	string recv_string = recvbuf;
	Json::Value value;
	bool success = reader.parse(recv_string, value, false);
	if(!success)
	{
		wclear(io_menu);
		wprintw(io_menu, "Failed to parse response");
		wrefresh(io_menu);
	}

	update_song_list(value, song_window);

	//TODO Find out if the const char * has to be freed
}

void handle_search(WINDOW *io_menu,
					WINDOW *song_window
					)
{
	//TODO For now, only searching by song title
	char buf[100];

	wclear(io_menu);
	wprintw(io_menu, "Song title : ");
	curs_set(1);
	echo();

	wgetstr(io_menu, buf);

	noecho();
	curs_set(0);
	wrefresh(io_menu);


	Json::Value value = generate_search_json(buf);

	do_search(io_menu, song_window, value);
	wclear(io_menu);
	wprintw(io_menu, "Done searching");
	wrefresh(io_menu);
}

void register_own_songs(WINDOW *io_menu)
{
	Json::FastWriter writer;
	const char *to_send = writer.write(initial_list_root).c_str();
	int bytes_to_send = strlen(to_send);
	char recvbuf[100];

	//TODO wrap all socket send/receive calls in a decent wrapper
	//Since we know all payload data is JSON
	//Create a socket, transmit JSON object over it
	int sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if(sockfd < 0)
	{
		wclear(io_menu);
		wprintw(io_menu, "Failed to create socket");
		wrefresh(io_menu);
	}

	if(connect(sockfd, (struct sockaddr *)&current_server_registration_address, sizeof(struct sockaddr)) < 0)
	{
		wclear(io_menu);
		wprintw(io_menu, "Failed to connect to server");
		wrefresh(io_menu);
		return;
	}
	int bytes_sent = 0;
	//Keep sending until all bytes sent
	//TODO Error handling needed here
	while(bytes_to_send > 0)
	{
		bytes_sent = send(sockfd, to_send + bytes_sent, bytes_to_send, 0);
		bytes_to_send -= bytes_sent;
	}
	bzero(recvbuf, 100);
	//Close the write end of the socket so that the server receives an
	//end of stream
	shutdown(sockfd, SHUT_WR);
	int bytes_received = 0;
	//TODO Buffer protection needed here
	while(true)
	{
		bytes_received = recv(sockfd, recvbuf + bytes_received, 1024, 0);
		if(bytes_received == 0)	//Server closed connection
		{
			break;
		}
	}
	close(sockfd);

	//Now parse the payload
	Json::Reader reader;
	string recv_string = recvbuf;
	Json::Value value;
	bool success = reader.parse(recv_string, value, false);
	if(!success)
	{
		wclear(io_menu);
		wprintw(io_menu, "Failed to parse response");
		wrefresh(io_menu);
	}

	//TODO Check for OK in response payload
	wclear(io_menu);
	wprintw(io_menu, "File refresh complete");
	wrefresh(io_menu);
}

void handle_refresh(WINDOW *io_menu,
						WINDOW *song_window)
{
	//Create an empty JSON object and pass for search
	char buf[20];

	bzero(buf, 20);
	wclear(io_menu);
	wprintw(io_menu, "IP Address : ");
	wrefresh(io_menu);
	curs_set(1);
	echo();

	//Try to get input
	wgetstr(io_menu, buf);

	//Attempt to parse it
	//TODO: If no IP entered, use old one only
	if(inet_aton(buf, &current_server_registration_address.sin_addr) == 0)
	{
		//Display an error
		wclear(io_menu);
		wprintw(io_menu, "Unable to parse IP : %s" , buf);
		wrefresh(io_menu);
	}
	else
	{
		inet_aton(buf, &current_server_lookup_address.sin_addr);
		inet_aton(buf, &current_server_peer_address.sin_addr);
		//Send own file list to server
		register_own_songs(io_menu);
		Json::Value empty_json_object(Json::objectValue);
		empty_json_object["Title"] = "";
		do_search(io_menu, song_window, empty_json_object);
	}
	curs_set(0);
	noecho();

}

void handle_register_song()
{
	//TODO Take a single song, add it to registration file, also send to server

}

void handle_unregister_song()
{
	//TODO Take song key/choose song, remove from list
}

Json::Value send_peer_list_request(WINDOW *io_menu, string key)
{
	Json::FastWriter writer;
	Json::Value sendValue(Json::objectValue);
	sendValue["Key"] = key;
	const char *to_send = writer.write(sendValue).c_str();
	int bytes_to_send = strlen(to_send);
	char recvbuf[1000];

	//TODO wrap all socket send/receive calls in a decent wrapper
	//Since we know all payload data is JSON
	//Create a socket, transmit JSON object over it
	int sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if(sockfd < 0)
	{
		wclear(io_menu);
		wprintw(io_menu, "Failed to create socket");
		wrefresh(io_menu);
	}

	if(connect(sockfd, (struct sockaddr *)&current_server_peer_address, sizeof(struct sockaddr)) < 0)
	{
		wclear(io_menu);
		wprintw(io_menu, "Failed to connect to server");
		wrefresh(io_menu);
		return Json::arrayValue;
	}
	int bytes_sent = 0;
	//Keep sending until all bytes sent
	//TODO Error handling needed here
	while(bytes_to_send > 0)
	{
		bytes_sent = send(sockfd, to_send + bytes_sent, bytes_to_send, 0);
		bytes_to_send -= bytes_sent;
	}
	bzero(recvbuf, 100);
	//Close the write end of the socket so that the server receives an
	//end of stream
	shutdown(sockfd, SHUT_WR);
	int bytes_received = 0;
	//TODO Buffer protection needed here
	while(true)
	{
		bytes_received = recv(sockfd, recvbuf + bytes_received, 1000 - bytes_received, 0);
		if(bytes_received == 0)	//Server closed connection
		{
			break;
		}
	}
	close(sockfd);

	//Now parse the payload
	Json::Reader reader;
	string recv_string = recvbuf;
	Json::Value value;
	bool success = reader.parse(recv_string, value, false);
	if(!success)
	{
		wclear(io_menu);
		wprintw(io_menu, "Failed to parse response");
		wrefresh(io_menu);
	}

	//TODO Check for OK in response payload
	//wclear(io_menu);
	//wprintw(io_menu, "Peer list fetched");
	//wrefresh(io_menu);

	PRINT_DEBUG("Peer list fetched\n");

	return value;
}

void handle_song_selection(char input_character, WINDOW *io_menu)
{
	int numeric_input;
	if(input_character == '0')
	{
		numeric_input = 9;
	}
	else
	{
		numeric_input = input_character - '1';
	}
	int vector_index = numeric_input + SONGS_PER_PAGE * current_song_list_page;

	if(vector_index + 1 > song_list.size() )
	{
		//Illegal input
		wclear(io_menu);
		wprintw(io_menu, "Illegal song choice");
		wrefresh(io_menu);
		return;
	}


	string song_key = key_list[vector_index];

	Json::Value array = send_peer_list_request(io_menu, song_key);

	Json::FastWriter writer;
	string asString = writer.write(array);
	const char *cstring = asString.c_str();
	//wclear(io_menu);
	//wprintw(io_menu, "Peers fetched : %s", cstring);
	//wrefresh(io_menu);

	PRINT_DEBUG("Peers fetched : %s\n", cstring);
	Json::Value value(Json::objectValue);
	value["IP"] = array[0]["IP"];
	value["Key"] = song_key;
	value["Port"] = "9501";

	send_play_request(value);
}

void exit_callback(int signal)
{
	//Called when SIGINT or 'q' passed
	//Should signal children to die as well
	PRINT_DEBUG("Signalling children to terminate as well\n");
	kill(socket_manager_pid, SIGINT);
	kill(player_pid, SIGINT);
	PRINT_DEBUG("Sent signals. Press any key to exit\n");
	//Should ideally only exit through Q
	//SIGINT will break curses
}

bool process_input_character(char input_character,
								WINDOW *io_menu,
								WINDOW *menu_window,
								WINDOW *song_window)
{
	switch(input_character)
	{
	case QUIT :
		return true;

	case CONNECT:
		handle_connect(io_menu);
		break;
	case DISCONNECT:
		handle_disconnect(io_menu);
		break;
	case REFRESH:
		handle_refresh(io_menu, song_window);
		break;
	case NEXT_PAGE:
		handle_next_page(song_window, io_menu);
		break;
	case PREV_PAGE:
		handle_prev_page(song_window, io_menu);
		break;
	case REGISTER_SONG:
		handle_register_song();
		break;
	case UNREGISTER_SONG:
		break;
	case SEARCH_SONG:
		handle_search(io_menu, song_window);
		break;
	case '0':
	case '1':
	case '2':
	case '3':
	case '4':
	case '5':
	case '6':
	case '7':
	case '8':
	case '9':
		handle_song_selection(input_character, io_menu);
		break;
	default:
		wclear(io_menu);
		wprintw(io_menu, "Invalid input");
		wrefresh(io_menu);
	}
	return false;
}

void process_initial_song_list(WINDOW *io_window)
{
	char file_path[100];
	bzero(file_path, 100);

	//Create an empty array
	Json::Value *new_element;

	wclear(io_window);
	echo();
	curs_set(1);
	wprintw(io_window, "Enter path to song list file : ");
	//Get file name
	wscanw(io_window, "%s", file_path);

	//Format of song file - path, artist, title
	//TODO later extract metadata from the MP3 itself
	char file_buffer[200];
	FILE *f_ptr = fopen(file_path, "r");
	int str_len;
	char *artist, *song, *path;
	int counter;

	if(f_ptr == NULL)
	{
		//Error handling
		wclear(io_window);
		wprintw(io_window, "Failed to open file");
		wrefresh(io_window);
	}
	else
	{
		while(true)
		{
			bzero(file_buffer, 200);
			//TODO double check buffer safety here (200 or 199?)
			//TODO clean up all these hardcoded constants
			fgets(file_buffer,200, f_ptr);
			if(feof(f_ptr))
			{
				break;
			}
			str_len = strlen(file_buffer);

			//Trim \n
			//TODO Make it a separate function
			for(counter = 0; counter < str_len; counter++)
			{
				if(file_buffer[counter] == '\n')
				{
					file_buffer[counter] = '\0';
				}
			}

			path = strtok(file_buffer, "|");
			artist = strtok(NULL, "|");
			song = strtok(NULL, "|");
			string key_string = song;
			key_string += "_";
			key_string += artist;
			string path_string = path;

			std::transform(key_string.begin(), key_string.end(), key_string.begin(), ::tolower);


			//Insert into the map
			song_map[key_string] = path_string;

			//TODO Remove these debug prints
			//wclear(io_window);
			//wprintw(io_window, "Inserted %s" , key_string.c_str());
			//wrefresh(io_window);

			PRINT_DEBUG("Inserted <%s,%s> into map\n", key_string.c_str(), path_string.c_str());

			new_element = new Json::Value(Json::objectValue);
			(*new_element)["Artist"] = artist;
			(*new_element)["Title"] = song;
			//Append to JSON Array

			initial_list_root.append(*new_element);
			element_list.push_back(new_element);
		}
	}

	//TODO Deallocate array_elements once sent for the first time

	curs_set(0);
	noecho();
	wrefresh(io_window);
}

void socket_manager_request_handler(int signum)
{

	char pipebuf[200];
	bzero(pipebuf, 200);
	if(read(socket_manager_request_pipefd, pipebuf, 200) == 0)
	{
		return;
	}
	PRINT_DEBUG("Received request from socket manager\n");

	string lookup_key = pipebuf;
	PRINT_DEBUG("Key is %s\n", pipebuf);
	string song_path =  song_map[lookup_key];
	PRINT_DEBUG("Requested path is %s\n", song_path.c_str());
	Json::Value value(Json::objectValue);
	value["Path"] = song_path;

	Json::FastWriter writer;
	string value_string = writer.write(value);
	PRINT_DEBUG("Response to socket manager : %s\n", value_string.c_str());

	write(socket_manager_result_pipefd, value_string.c_str(), value_string.length());
	PRINT_DEBUG("Processed socket manager request\n");
}

int main()
{
	initscr();
	WINDOW *song_list_window;
	WINDOW *menu_window;
	WINDOW *input_output_window;

	signal(SIGINT, exit_callback);

	cbreak();
	noecho();
	curs_set(0);

	int startx, starty, width, height;
	height = LINES/2;
	width = COLS/2;
	startx = 0;
	starty = LINES/2;

	song_list_window = newwin(height-1, width-1, starty, startx);
	menu_window = newwin(height, width, 0, 0);
	input_output_window = newwin(1, COLS, LINES-1, 0);
	debug_window = newwin(LINES, COLS/2, 0, COLS/2);

	scrollok(debug_window, true);
	idlok(debug_window, true);


	wattrset(input_output_window, A_BOLD);

	current_server_registration_address.sin_family = AF_INET;
	current_server_registration_address.sin_port = htons(REGISTER_PORT);
	current_server_lookup_address.sin_family = AF_INET;
	current_server_lookup_address.sin_port = htons(SEARCH_PORT);
	current_server_peer_address.sin_family = AF_INET;
	current_server_peer_address.sin_port = htons(GET_PEER_PORT);

	refresh();

	wprintw(song_list_window, "Song list\n");
	wprintw(menu_window, "Menu\n");

	wprintw(input_output_window, "I/O");
	display_menu(menu_window);

	wrefresh(song_list_window);
	wrefresh(menu_window);
	wrefresh(input_output_window);
	char input_character;
	bool exit_flag = false;



	PRINT_DEBUG("---DEBUGGING---\n");


	mkfifo("input_pipe", 0666);
	mkfifo("result_pipe", 0666);
	mkfifo("PlayerCommandPipe", 0666);

	signal(SIGIO, socket_manager_request_handler);

	//Open input_pipe in O_ASYNC mode
	socket_manager_request_pipefd = open("input_pipe", O_RDONLY | O_NONBLOCK | O_CLOEXEC);
	if(socket_manager_request_pipefd < 0)
	{
		PRINT_DEBUG("Failed to open command pipe\n");
		getch();
		endwin();
		exit(1);
	}

	/* set socket owner (the process that will receive signals) */
	fcntl(socket_manager_request_pipefd, F_SETOWN, getpid());

	/* optional if you want to receive a real-time signal instead of SIGIO */
	/* turn on async mode -- this is the important part which enables signal delivery */
	fcntl(socket_manager_request_pipefd, F_SETFL, fcntl(socket_manager_request_pipefd, F_GETFL, 0) | O_ASYNC);

	//Fork socket manager and player processes
	socket_manager_pid = fork();
	if(!socket_manager_pid)
	{
		//Create socket manager
		execl("./SocketManager", "SocketManager");
	}
	PRINT_DEBUG("Created socket manager\n");

	player_pid = fork();
	if(!player_pid)
	{
		//Create player
		execl("./Player", "Player");
	}
	PRINT_DEBUG("Created Player\n");


	do
	{
		socket_manager_result_pipefd = open("result_pipe", O_WRONLY | O_NONBLOCK | O_CLOEXEC);
	}
	while(socket_manager_result_pipefd < 0);

	PRINT_DEBUG("Opened Socket Manager Result pipe\n");

	do
	{
		player_command_pipefd = open("PlayerCommandPipe", O_WRONLY | O_NONBLOCK | O_CLOEXEC);
	//	PRINT_DEBUG("Failed to open result manager pipe\n");
	}
	while(player_command_pipefd < 0);

	PRINT_DEBUG("Pipes created\n");


	process_initial_song_list(input_output_window);

	do
	{
		input_character = getch();
		exit_flag =  process_input_character(input_character, input_output_window, menu_window, song_list_window);
	}
	while(!exit_flag);

	exit_callback(0);
	//PRINT_DEBUG("Press any key to exit\n");
	getch();
	endwin();

	return 0;
}
