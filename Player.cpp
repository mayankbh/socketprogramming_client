#include <stdio.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <netinet/in.h>
#include <iostream>
#include <fstream>
#include <signal.h>
#include <stdlib.h>
#include <strings.h>
#include <cstring>
#include <json/json.h>
#include <json/writer.h>
#include <json/reader.h>
#include <arpa/inet.h>
#include <poll.h>

using namespace std;
fstream logger_stream;
int command_pipe_fd;
Json::Reader reader;
Json::FastWriter writer;
string song_key;
string command;
string my_ip;

pid_t madplayer_pid;

struct sockaddr_in peer_addr;

volatile unsigned int pending_command_map = 0;
volatile bool exit_flag = false;

#define PLAY 1

#define READ 0
#define WRITE 1

void close_logger_and_exit(int err)
{
	logger_stream.close();
	exit(err);
}

void exit_handler(int signal)
{
	logger_stream << "Received SIGINT from parent" << endl;
	//TODO Figure out how to remove this goto
	//Probably a volatile flag can be set and looped on in main
	exit_flag = true;
}

void pipe_signal_handler(int signal)
{
	//Called whenever main process writes into pipe
	logger_stream << "Entering signal handler\n";
	char pipe_buffer[200];
	Json::Value value(Json::objectValue);
	bzero(pipe_buffer, 200);
	read(command_pipe_fd, pipe_buffer, 200);
	//Command is a JSON containing Key
	if(!reader.parse(pipe_buffer, value, false))
	{
		logger_stream << "Failed to parse command JSON from main process" << endl;
		return;
	}

	logger_stream << "Received " << pipe_buffer << endl;

	//TODO Assert that JSON is properly formed
	song_key = value["Key"].asString();
//	if(song_key == NULL)
//	{
//		logger_stream << "Song key not present in command word" << endl;
//		return;
//	}
	command = value["Command"].asString();
//	if(command == NULL)
//	{
//		logger_stream << "Command not present in command word" << endl;
//		return;
//	}

	string ip = value["IP"].asString();
//	if(ip == NULL)
//	{
//		logger_stream << "IP not present in command word" << endl;
//		return;
//	}



	if(command == "Play")
	{
		logger_stream << "Setting PLAY flag" << endl;
		pending_command_map |= PLAY;
		inet_aton(ip.c_str(), &peer_addr.sin_addr);
		peer_addr.sin_family = AF_INET;
		peer_addr.sin_port = htons(9500);
	}

	logger_stream << "Exiting signal handler" << endl;
}

void reset_chunk_map(char chunk_map[])
{
	for(int i = 0; i < 16383; i++)
	{
		chunk_map[i] = 0;
	}
}

int main()
{
	logger_stream.open("PlayerLog.log", ios::out);
	int socket_fd;
	struct pollfd udp_socket_pollfd[1];

	int filedes[2];
	pipe(filedes);
	int filedes2[2];
	pipe(filedes2);

	pid_t pid = fork();
	if (pid == 0)
	{
		close(filedes[WRITE]);
		close(filedes2[READ]);
		dup2(filedes[READ], 0);
		dup2(filedes2[WRITE], 1);

		char *argv[] = {"madplay", "-", "-o", "wave:-", NULL };
		execv("./madplay", argv);
		exit(0);
	} 
	else
	{
		close(filedes[READ]);
		dup2(filedes[WRITE], 1);
	}

	pid = fork();
	if (pid == 0)
	{
		close(filedes2[WRITE]);
		dup2(filedes2[READ], 0);
		char *argv[] = {"aplay", "--period-size=2000", "--buffer-size=10000", NULL};
		execv("/usr/bin/aplay", argv);
	}
	else
	{
		close(filedes2[READ]);
		close(filedes2[WRITE]);
	}


	//Install signal handler
	signal(SIGIO, pipe_signal_handler);

	//TODO Configure pipe for O_ASYNC

	command_pipe_fd = open("PlayerCommandPipe", O_RDONLY | O_NONBLOCK);
	if(command_pipe_fd < 0)
	{
		logger_stream << "Failed to open command pipe" << endl;
		close_logger_and_exit(1);
	}

	/* set socket owner (the process that will receive signals) */
	fcntl(command_pipe_fd, F_SETOWN, getpid());

	/* optional if you want to receive a real-time signal instead of SIGIO */
	/* turn on async mode -- this is the important part which enables signal delivery */
	fcntl(command_pipe_fd, F_SETFL, fcntl(command_pipe_fd, F_GETFL, 0) | O_ASYNC);

	socket_fd = socket(AF_INET, SOCK_DGRAM, 0);
	if(socket_fd < 0)
	{
		logger_stream << "Unable to create socket" << endl;
		close_logger_and_exit(1);
	}

	udp_socket_pollfd[0].events = POLLIN;
	udp_socket_pollfd[0].fd = socket_fd;

	struct sockaddr_in my_addr;
	my_addr.sin_addr.s_addr = INADDR_ANY;
	my_addr.sin_port = htons(9501);
	my_addr.sin_family = AF_INET;
	my_ip = "127.0.0.1";

	char recv_buf[1028];
	char chunk_map[16384];


	reset_chunk_map(chunk_map);


	int command_fd;

	if(bind(socket_fd, (struct sockaddr *)&my_addr, sizeof(struct sockaddr)) < 0)
	{
		logger_stream << "Unable to bind socket" << endl;
		close_logger_and_exit(1);
	}
	logger_stream << "Successfully bound socket " << endl;

	//TODO Ensure datagram is from expected peer and not anyone else
	unsigned int chunk_index = 0;
	unsigned int network_chunk_index;

	while(!(pending_command_map & PLAY));	//Wait for play request

	while(true)
	{
		//Event loop

		reset_chunk_map(chunk_map);
		pending_command_map = 0;
		int command_socket = socket(AF_INET, SOCK_STREAM, 0);
		if(command_socket < 0)
		{
			logger_stream << "Failed to create command socket" << endl;
			continue;
		}

		if(connect(command_socket, (struct sockaddr *)&peer_addr, sizeof(struct sockaddr)))
		{
			logger_stream << "Failed to connect to peer" << endl;
			continue;
		}
		logger_stream << "Successfully connected to peer" << endl;

		Json::Value value(Json::objectValue);
		Json::FastWriter writer;
		value["Key"] = song_key;

		//Shouldn't have to send IP in JSON
		//Unnecessary network traffic only
		//value["IP"] = my_ip;
		value["Port"] = "9501";

		string request_string = writer.write(value);
		const char *request_cstring = request_string.c_str();

		int bytes_sent = 0;

		while(bytes_sent < request_string.length())	//Send entire JSON
		{
			bytes_sent += write(command_socket, request_cstring + bytes_sent, request_string.length() - bytes_sent);
		}
		close(command_socket);
		logger_stream << "Sent request" << request_cstring << endl;

		//TODO Replace this with a poll
		while(pending_command_map == 0)	//Until new request comes
		{
			bzero(recv_buf, 1028);
			//read(socket_fd, recv_buf, 1028);
			if(exit_flag)
			{
				logger_stream << "Exit flag is set" << endl;
				break;
			}

			int poll_ret = poll(udp_socket_pollfd, 1, 200);

			if(pending_command_map != 0)
			{
				break;
			}

			//TODO implement timeout here, maybe if didn't receive chunks for
			//10 cycles
			if(poll_ret != 0)
			{
//				logger_stream << "Preparing to enter read()" << endl;
				int recvd = recvfrom(socket_fd, recv_buf, 1028, 0, NULL, NULL);
				memcpy(&network_chunk_index, recv_buf, 4);
				chunk_index = ntohl(network_chunk_index);
				chunk_map[chunk_index] = 1;


				logger_stream << "Received chunk " << chunk_index << endl;

				logger_stream << "Writing chunk " << chunk_index << " to STDOUT" << endl;
				write(1, recv_buf + 4, recvd - 4);
			}
			else
			{
//				logger_stream << "No data in socket" << endl;
			}
		}

		logger_stream << "Detected changed pending command map" << endl;
	}

	close_fds:

	logger_stream << "Reached close_fds" << endl;

	close(command_pipe_fd);
	close(socket_fd);
	logger_stream.close();
}
