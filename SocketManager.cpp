#include <stdio.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <netinet/in.h>
#include <iostream>
#include <fstream>
#include <json/json.h>
#include <json/writer.h>
#include <json/reader.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <cstring>
#include <errno.h>
#include <arpa/inet.h>

#define LISTEN_PORT 9500	//TCP
#define QUERY_INPUT_PIPE "input_pipe"
#define QUERY_RESULT_PIPE "result_pipe"

using namespace std;
vector<int> child_list;
fstream logger_stream;

volatile int socket_fd;

volatile bool exit_flag = false;

void child_death_handler(int)
{
	logger_stream << "Received SIGCHLD" << endl;

	pid_t pid;
	while((pid = waitpid(-1, NULL, WNOHANG)) != -1)
	{
		for( vector<int>::iterator iter = child_list.begin(); iter != child_list.end(); ++iter )
		{
		    if( *iter == pid )
		    {
		        child_list.erase( iter );
		        break;
		    }
		}
		logger_stream << "Child " << pid << " died" << endl;
	}
}

void close_logger_and_exit(int err)
{
	logger_stream.close();
	exit(err);
}

//TODO:Disable SIGCHLD handler in here, since child might die after parent requests
//this process to exit
void exit_callback(int signal)
{
	logger_stream << "Received SIGINT from parent" << endl;
	pid_t pid;

	for( vector<int>::iterator iter = child_list.begin(); iter != child_list.end(); ++iter )
	{
		logger_stream << "Sending SIGINT to child " << *iter << endl;
		kill(*iter, SIGINT);
	}

	//TODO Figure out how to replace this goto

	logger_stream << "Setting exit flag " << endl;
	exit_flag = true;

	shutdown(socket_fd, SHUT_RDWR);


}

int main()
{
	logger_stream.open("SocketManagerLog.log", ios::out);
	signal(SIGCHLD, child_death_handler);
	signal(SIGINT, exit_callback);

	int read_pipe_fd;
	int write_pipe_fd;
	Json::Reader reader;
	Json::FastWriter writer;

	//int socket_fd;
	socket_fd = socket(AF_INET, SOCK_STREAM, 0);
	if(socket_fd < 0)
	{
		logger_stream << "Failed to create socket" << endl;
	}

	struct sockaddr_in my_addr;
	my_addr.sin_family = AF_INET;
	my_addr.sin_port = htons(LISTEN_PORT);
	my_addr.sin_addr.s_addr = INADDR_ANY;

	if(bind(socket_fd, (struct sockaddr *)&my_addr, sizeof(struct sockaddr)) < 0)
	{
		logger_stream << "Failed to bind socket" << endl;
	}

	if(listen(socket_fd, 5) < 0)
	{
		logger_stream << "Failed to initiate listen mode" << endl;
	}

	int connfd;
	struct sockaddr_in client_addr;
	unsigned int client_addr_len;

	char recv_buf[1000];
	char pipe_buf[1000];

	int bytes_received;
	Json::Value value;
	const char *cstring;

	write_pipe_fd = open(QUERY_INPUT_PIPE, O_WRONLY | O_CLOEXEC);
	if(write_pipe_fd < 0)
	{
		logger_stream << "Failed to open write pipe" << endl;
		close_logger_and_exit(1);
	}

	read_pipe_fd = open(QUERY_RESULT_PIPE, O_RDONLY | O_CLOEXEC);
	if(read_pipe_fd < 0)
	{
		logger_stream << "Failed to open read pipe" << endl;
		close_logger_and_exit(1);
	}
	Json::Value value_response;

	while(true)
	{
		value = Json::objectValue;
		value_response = Json::objectValue;

		client_addr_len = sizeof(struct sockaddr);
		bzero(&client_addr, sizeof(struct sockaddr));
		bzero(recv_buf, 1000);
		bzero(pipe_buf, 1000);

		//TODO Check the EINTR was not returned
		connfd = accept(socket_fd, (struct sockaddr *) &client_addr, &client_addr_len);


		if(exit_flag)
		{
			logger_stream << "Exit flag set to true, exiting loop" << endl;
			break;
		}
		else
		{
			logger_stream << "Exit flag not set, okay to proceed" << endl;
		}


		do
		{
			bytes_received = read(connfd, recv_buf + bytes_received, 1000 - bytes_received);
		}
		while(bytes_received != 0);
		close(connfd);

		//Try parsing the JSON
		//Ignore request if malformed

		if(!reader.parse(recv_buf, value, false))
		{
			logger_stream << "Failed to parse JSON" << endl;
			continue;
		}

		//TODO Assert that JSON is well formed

//		if(value["Key"] == NULL)
//		{
//			logger_stream << "Song key not present in JSON" << endl;
//			continue;
//		}

//		if(value["IP"] == NULL)
//		{
//			logger_stream << "IP not present in JSON" << endl;
//			continue;
//		}

//		if(value["Port"] == NULL)
//		{
//			logger_stream << "IP not present in JSON" << endl;
//			continue;
//		}

		//Write key into pipe
		cstring = value["Key"].asCString();
		write(write_pipe_fd, cstring, strlen(cstring));

		//Close pipe

		//Open pipe for reading

		read(read_pipe_fd, pipe_buf, 1000);

		string client_ip = inet_ntoa(client_addr.sin_addr);

		//Try to parse JSON
		if(!reader.parse(pipe_buf, value_response, false))
		{
			logger_stream << "Unable to parse reply from Main process" << endl;
			logger_stream << "Received " << pipe_buf << endl;
			continue;
		}

		//if(value["Path"].asString() == NULL)
	//	{
//			logger_stream << "Main process response doesn't contain Path" << endl;
	//		continue;
	//	}
		logger_stream << "JSON Request is " << writer.write(value) << endl;

		//Now fork() and give new process the IP and port to stream to
		int pid = fork();
		if(pid == 0)
		{
			//Child executes new process
			const char *argv[4];
			argv[0] = "Streamer";
			argv[1] = value_response["Path"].asCString();
			//TODO Get IP from client_addr rather than embed in request itself
//			argv[2] = value["IP"].asCString();
			argv[2] = client_ip.c_str();
			argv[3] = value["Port"].asCString();
			logger_stream << "execl " << "./Streamer" << argv[0] << " " << argv[1] << " " << argv[2] << " " << argv[3] << endl;
			execl("./Streamer", argv[0], argv[1], argv[2], argv[3], (char *) 0);
//			execv("./Streamer", argv);
			//Problem seems to be because of the arguments being variable
//			execl("./Streamer", "./Streamer");
			cout << "Error : " << strerror(errno) << endl;
			logger_stream << "If this is printed, exec() didn't work" << endl;
		}
		else
		{
			//Add to list of children
			child_list.push_back(pid);
		}

	}

close_fds:

	close(read_pipe_fd);
	close(write_pipe_fd);
	close(socket_fd);

	logger_stream.close();
}
