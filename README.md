# README #

### What is this repository for? ###

* An implementation of a peer to peer music streaming client for our socket programming project, in Network Programming, for the 1st semester 2015-16.


### How do I get set up? ###


### Dependencies ###

*    JSONCPP
*    Madplay
*    APlay

These should fix most dependencies (apart from Madplay which needs to compiled and put into working dir manually):

sudo apt-get install libmad0-dev;

sudo apt-get install libid3tag0-dev;

sudo apt-get install libncurses-dev;

sudo apt-get install libjsoncpp-dev;

sudo apt-get install libboost-all-dev;