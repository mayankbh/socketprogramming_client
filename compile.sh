g++ -lncurses  -ljsoncpp  SocketProgramming_Project_Client.cpp -o Main -g;
g++ -ljsoncpp  Streamer.cpp -o Streamer -g;
g++ -ljsoncpp  Player.cpp -o Player -g;
g++ -ljsoncpp  SocketManager.cpp -o SocketManager -g;

chmod +x Main;
chmod +x Streamer;
chmod +x Player;
chmod +x SocketManager;

rm input_pipe;
rm result_pipe;
rm PlayerCommandPipe;
rm *.log;

pkill SocketManager;
pkill Streamer;
pkill Player;
