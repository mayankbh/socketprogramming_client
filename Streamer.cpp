#include <stdio.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <netinet/in.h>
#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <arpa/inet.h>
#include <strings.h>
#include <cstring>
#include <boost/lexical_cast.hpp>

using namespace std;
fstream logger_stream;
volatile bool exit_flag = false;

void close_logger_and_exit(int err)
{
	logger_stream.close();
	exit(err);
}

void exit_callback(int signal)
{
	logger_stream << "Received SIGINT from parent" << endl;

	//TODO Figure out how to remove this goto
	exit_flag = true;
}

int main(int argc, char *argv[])
{
	int pid = getpid();

	char logfile_name[100];
	sprintf(logfile_name, "StreamerLog%d.log", pid);

	logger_stream.open(logfile_name, ios::out);
	//argv[0] = ./Streamer
	//argv[1] = Path to MP3
	//argv[2] = IP to stream to
	//argv[3] = Port to stream to

	logger_stream << "Streamer spawned, streaming " << argv[1] << " to " << argv[2] << ":" << argv[3] << endl;

	if(argc != 4)
	{
		logger_stream << "Invalid argument count. Streamer is exiting" << endl;
		logger_stream << "Received " << argc << ", expected " << 4 << endl;
		close_logger_and_exit(1);
	}

	int socket_fd = socket(AF_INET, SOCK_DGRAM, 0);
	if(socket_fd < 0)
	{
		logger_stream << "Unable to create socket" << endl;
		close_logger_and_exit(1);
	}

	struct sockaddr_in peer_addr;
	if(inet_aton(argv[2], &peer_addr.sin_addr) == 0)
	{
		logger_stream << "Unable to parse IP address" << endl;
		close_logger_and_exit(1);
	}
	peer_addr.sin_port = htons(atoi(argv[3]));
	peer_addr.sin_family = AF_INET;

	int file_fd = open(argv[1], O_RDONLY);
	if(file_fd < 0)
	{
		logger_stream << "Unable to open file for streaming" << endl;
		close_logger_and_exit(1);
	}

	//1024 byte chunk, 4 byte chunk number
	char send_buffer[1028];
	unsigned int chunk_number = 0;
	unsigned int network_chunk_number = 0;
	unsigned int bytes_read = 0;
	//Maintain a bitmap to reconstruct MP3 on other end


	while(!exit_flag)
	{
		bzero(send_buffer, 1028);
		network_chunk_number = htonl(chunk_number);
		chunk_number++;
		bytes_read = read(file_fd, send_buffer + 3, 1024);
		memcpy(send_buffer, &network_chunk_number, 4);
		sendto(socket_fd, send_buffer, 1028, 0, (struct sockaddr *)&peer_addr, sizeof(struct sockaddr));


		if(0 == bytes_read)
		{
			logger_stream << "Finished transferring file" << endl;
			break;
		}

	}

	close_fds:

	close(file_fd);
	close(socket_fd);

	logger_stream.close();

	exit(0);
}
